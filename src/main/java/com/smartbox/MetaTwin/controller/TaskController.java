package com.smartbox.MetaTwin.controller;

import com.smartbox.MetaTwin.History;
import com.smartbox.MetaTwin.Task;
import com.smartbox.MetaTwin.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class TaskController {

    @Autowired
    TaskRepository taskRepository;

    @GetMapping("task")
    public List<Task> getAllTasks(){
        List<Task> tasks = taskRepository.findAll();
        return  tasks;
    }

    @PostMapping(value = "task")
    public String insertTask(@RequestBody Task task) {
        taskRepository.save(task);
        return "Successfully Inserted";
    }

    @PostMapping(value = "task/{taskId}")
    public Optional<Task> getTaskById(@PathVariable String taskId) {
        Optional<Task> task = taskRepository.findById(taskId);
        return task;
    }

    @PutMapping(value = "task/{taskId}/{active}")
    public Task changeStatus(@PathVariable String taskId, @PathVariable Boolean active) {
        Task task = taskRepository.findById(taskId).get();
        task.setActive(active);
        taskRepository.save(task);
        return task;
    }

    @DeleteMapping("/task/{taskId}")
    public void deleteTask(@PathVariable String taskId){
        taskRepository.deleteById(taskId);
    }

}
