package com.smartbox.MetaTwin.controller;

import com.smartbox.MetaTwin.History;
import com.smartbox.MetaTwin.repository.HistoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class HistoryController {

    @Autowired
    HistoryRepository historyRepository;

    @GetMapping("/history")
    public List<History> getAllHistory(History history) {
        List<History> histories = historyRepository.findAll();
        return histories;
    }

    @RequestMapping(value = {"/history/{id}"})
    public  Optional<History> getHistoryFilteredById(@PathVariable Integer id) {
        Optional<History> history = historyRepository.findById(id);
        return history;
    }

    @RequestMapping(value = {"/history/find/{columnValue}"})
    public List<History> getHistoryFilteredByParameter(@PathVariable String columnValue) {
        List<History> histories = historyRepository.getHistoryByParameter(columnValue);
        return histories;
    }

    @PostMapping(value = "/history")
    public String insertHistory(@RequestBody History history) {
        historyRepository.save(history);
        return "Successfully Inserted";
    }
}
