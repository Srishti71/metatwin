package com.smartbox.MetaTwin.controller;

import com.smartbox.MetaTwin.Files;
import com.smartbox.MetaTwin.repository.FileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class FileController {

    @Autowired
    FileRepository fileRepository;

    @GetMapping("file")
    public List<Files> getAllFiles(){
        List<Files> files = fileRepository.findAll();
        return  files;
    }

    @PostMapping(value = "files")
    public String insertFile(@RequestBody Files files) {
        fileRepository.save(files);
        return "Successfully Inserted";
    }

    @PostMapping(value = "files/{filesId}")
    public Optional<Files> getFilesById(@PathVariable String fileId) {
        Optional<Files> files = fileRepository.findById(fileId);
        return files;
    }

    @DeleteMapping("files/{fileId}")
    public void deleteTask(@PathVariable String fileId){
        fileRepository.deleteById(fileId);
    }
}
