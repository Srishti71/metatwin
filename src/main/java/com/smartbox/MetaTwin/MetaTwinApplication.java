package com.smartbox.MetaTwin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MetaTwinApplication {

    public static void main(String[] args) {
        SpringApplication.run(MetaTwinApplication.class, args);
    }

}
