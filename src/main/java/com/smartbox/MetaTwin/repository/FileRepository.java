package com.smartbox.MetaTwin.repository;

import com.smartbox.MetaTwin.Files;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FileRepository extends JpaRepository<Files, String> {
}
