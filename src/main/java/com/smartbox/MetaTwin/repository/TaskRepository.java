package com.smartbox.MetaTwin.repository;


import com.smartbox.MetaTwin.Task;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TaskRepository extends JpaRepository<Task, String> {


}
