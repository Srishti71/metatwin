package com.smartbox.MetaTwin.repository;

import com.smartbox.MetaTwin.History;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface HistoryRepository extends JpaRepository<History, Integer> {

    @Query(value = "SELECT * FROM history where macAddress= :value or gatewayId= :value or timestamp= :value or rssi= :value", nativeQuery = true)
    List<History> getHistoryByParameter(String value);
}
