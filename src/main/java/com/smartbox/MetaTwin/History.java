package com.smartbox.MetaTwin;

import javax.persistence.*;

@Entity
public class History {

    @Id

    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @Column(name = "macAddress")
    private String macAddress;

    @Column(name = "gatewayId")
    private String gatewayId;

    @Column(name = "timestamp")
    private String timestamp;

    @Column(name = "rssi")
    private String rssi;

    public History() {

    }

    public History(String macAddress, String gatewayId, String timestamp, String rssi) {
        this.macAddress = macAddress;
        this.gatewayId = gatewayId;
        this.timestamp = timestamp;
        this.rssi = rssi;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public String getGatewayId() {
        return gatewayId;
    }

    public void setGatewayId(String gatewayId) {
        this.gatewayId = gatewayId;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getRssi() {
        return rssi;
    }

    public void setRssi(String rssi) {
        this.rssi = rssi;
    }
}
